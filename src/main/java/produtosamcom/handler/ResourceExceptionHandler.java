package produtosamcom.handler;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import produtosamcom.model.DetalhesErro;
import produtosamcom.service.exceptions.ProdutoNotFoundException;
import produtosamcom.service.exceptions.VendaNotFoundException;

@ControllerAdvice
public class ResourceExceptionHandler {
	
	@ExceptionHandler(ProdutoNotFoundException.class)
	public ResponseEntity<DetalhesErro> handleBookNotFoundException(
			ProdutoNotFoundException e, HttpServletRequest request
	) {
		DetalhesErro erro = new DetalhesErro();
		erro.setStatus(404L);
		erro.setMensagem("Produto não cadastrado");
		
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(erro);
	}
	
	
	
	@ExceptionHandler(VendaNotFoundException.class)
	public ResponseEntity<DetalhesErro> handleBookNotFoundException(
			VendaNotFoundException e, HttpServletRequest request
	) {
		DetalhesErro erro = new DetalhesErro();
		erro.setStatus(404L);
		erro.setMensagem("Venda não cadastrada");
		
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(erro);
	}
}
