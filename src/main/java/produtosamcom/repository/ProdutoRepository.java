package produtosamcom.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import produtosamcom.model.Produto;

public interface ProdutoRepository extends JpaRepository<Produto, Long> {
	
	@Query(value="SELECT SUM(p.preco) FROM Produto p WHERE id IN (:ids)", nativeQuery=true)
	public Float sumPrecosPerId(@Param("ids") List<Long> ids); 
}
