package produtosamcom.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import produtosamcom.model.Venda;

public interface VendaRepository extends JpaRepository<Venda, Long> {
	
}
