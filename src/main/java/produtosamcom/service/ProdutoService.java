package produtosamcom.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import produtosamcom.model.Produto;
import produtosamcom.repository.ProdutoRepository;
import produtosamcom.service.exceptions.ProdutoNotFoundException;

@Service
public class ProdutoService {
	@Autowired
	private ProdutoRepository produtoRepository;
	
	public List<Produto> findAll() {
		return produtoRepository.findAll();
	}
	
	
	
	public Produto findById(Long id) {
		java.util.Optional<Produto> produto = produtoRepository.findById(id);
		
		if(!produto.isPresent()) {
			throw new ProdutoNotFoundException("Produto encontrado");
		}
		
		return produto.get();
	}
	
	
	
	public Produto save(Produto produto) {
		produto.setId(null);
		return produtoRepository.save(produto);
	}
	
	
	
	public void update(Produto produto) {
		checkIfExists(produto);
		produtoRepository.save(produto);
	}
	
	
	
	public void checkIfExists(Produto produto) {
		findById(produto.getId());
	}
	
	
	
	public Float sumPrecosPerId(List<Long> ids) {
		for(Long id : ids) {
			findById(id);
		}
		return produtoRepository.sumPrecosPerId(ids);
	}
	
	
	
	public void deleteById(Long id) {
		try {
			produtoRepository.deleteById(id);
		}
		catch(EmptyResultDataAccessException e) {
			throw new ProdutoNotFoundException("Produto não encontrado");
		}
	}
}
