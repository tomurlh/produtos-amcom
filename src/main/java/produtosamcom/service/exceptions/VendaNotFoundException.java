package produtosamcom.service.exceptions;

public class VendaNotFoundException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public VendaNotFoundException(String message) {
		super(message);
	}
	
	public VendaNotFoundException(String message, Throwable reason) {
		super(message, reason);
	}
}
