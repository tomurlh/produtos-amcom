package produtosamcom.service.exceptions;

public class ItemNotFoundException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public ItemNotFoundException(String message) {
		super(message);
	}
	
	public ItemNotFoundException(String message, Throwable reason) {
		super(message, reason);
	}
}