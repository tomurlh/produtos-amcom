package produtosamcom.service.exceptions;

public class ProdutoNotFoundException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public ProdutoNotFoundException(String message) {
		super(message);
	}
	
	public ProdutoNotFoundException(String message, Throwable reason) {
		super(message, reason);
	}
}