package produtosamcom.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import produtosamcom.model.Produto;
import produtosamcom.model.Venda;
import produtosamcom.repository.VendaRepository;
import produtosamcom.service.exceptions.ItemNotFoundException;
import produtosamcom.service.exceptions.VendaNotFoundException;

@Service
public class VendaService {
	@Autowired
	private VendaRepository vendaRepository;
	@Autowired
	private ProdutoService produtoService;

	public List<Venda> findAll() {
		return vendaRepository.findAll();
	}
	
	
	
	public Venda findById(Long id) {
		java.util.Optional<Venda> documento = vendaRepository.findById(id);

		if(!documento.isPresent()) {
			System.out.println("não presente");
			throw new ItemNotFoundException("Venda não encontrada");
		}
		
		return documento.get();
	}
	
	
	
	public Venda save(Venda venda) {
		venda.setId(null);
		Float total = 0F;
		for(Produto p : venda.getProdutos()) {
			produtoService.checkIfExists(p);
			Produto pObject = produtoService.findById(p.getId());
			total = total + pObject.getPreco();
		}
		venda.setTotal(total);
		return vendaRepository.save(venda);
	}
	
	
	
	public void update(Venda venda) {
		checkIfExists(venda);
		
		Float total = 0F;
		for(Produto p : venda.getProdutos()) {
			produtoService.checkIfExists(p);
			Produto pObject = produtoService.findById(p.getId());
			total = total + pObject.getPreco();
		}
		venda.setTotal(total);
		
		vendaRepository.save(venda);
	}
	
	
	
	private void checkIfExists(Venda venda) {
		findById(venda.getId());
	}
	
	
	
	public void deleteById(Long id) {
		try {
			vendaRepository.deleteById(id);
		}
		catch(EmptyResultDataAccessException e) {
			throw new VendaNotFoundException("Venda não encontrada");
		}
	}
}
