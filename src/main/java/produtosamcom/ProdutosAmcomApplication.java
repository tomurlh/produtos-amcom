package produtosamcom;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProdutosAmcomApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProdutosAmcomApplication.class, args);
	}

}

