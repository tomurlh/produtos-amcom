package produtosamcom.controller;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import produtosamcom.model.Venda;
import produtosamcom.service.VendaService;

@RestController
@Api(value = "Venda", description = "CRUD de vendas")
@RequestMapping("/venda")
public class VendaController {
	@Autowired
	private VendaService vendaService;

	@CrossOrigin
	@ApiOperation(value = "Busca totas vendas")
	@RequestMapping(method=RequestMethod.GET)
	public ResponseEntity<List<Venda>> findAll() {
		return ResponseEntity.status(HttpStatus.OK).body(vendaService.findAll());
	}
	
	
	
	@CrossOrigin
	@ApiOperation(value = "Busca uma venda por ID")
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	public ResponseEntity<Venda> find(@PathVariable("id") Long vendaId) {
		Venda venda = vendaService.findById(vendaId);
		return ResponseEntity.status(HttpStatus.OK).body(venda);
	}		
	
	
	
	@ApiOperation(value = "Cadastra uma venda somando o total dos produtos")
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Void> save(@Valid @RequestBody Venda venda) {
		venda = vendaService.save(venda);
		
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
				.path("/{id}").buildAndExpand(venda.getId()).toUri();
		
		return ResponseEntity.created(uri).build();
	}
	
	
	
	@ApiOperation(value = "Atualiza uma venda e o total dos produtos")
	@RequestMapping(value="/{id}", method=RequestMethod.PUT)
	public ResponseEntity<Void> update(@RequestBody Venda venda, @PathVariable("id") Long id)  {
		venda.setId(id);
		vendaService.update(venda);
		return ResponseEntity.noContent().build();
	}
	
	
	
	@ApiOperation(value = "Remove uma venda por ID")
	@RequestMapping(value="/{id}", method=RequestMethod.DELETE)
	public ResponseEntity<Void> delete(@PathVariable("id") Long id)  {
		vendaService.deleteById(id);
		return ResponseEntity.noContent().build();
	}
}
