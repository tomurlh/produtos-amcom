package produtosamcom.controller;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import produtosamcom.model.Produto;
import produtosamcom.service.ProdutoService;

@RestController
@RequestMapping("/produto")
@Api(value = "Produto", description = "CRUD de produtos")
public class ProdutoController {
	@Autowired
	private ProdutoService produtoService;

	@CrossOrigin
	@ApiOperation(value = "Busca todos produtos")
	@RequestMapping(method=RequestMethod.GET)
	public ResponseEntity<List<Produto>> findAll() {
		return ResponseEntity.status(HttpStatus.OK).body(produtoService.findAll());
	}
	
	
	
	@CrossOrigin
	@ApiOperation(value = "Busca produto por ID")
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	public ResponseEntity<Produto> find(@PathVariable("id") Long produtoId) {
		Produto produto = produtoService.findById(produtoId);
		
		return ResponseEntity.status(HttpStatus.OK).body(produto);
	}
	
	
	
	@ApiOperation(value = "Cadastra um produto")
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Void> save(@Valid @RequestBody Produto produto) {
		produto = produtoService.save(produto);
		
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
				.path("/{id}").buildAndExpand(produto.getId()).toUri();
		
		return ResponseEntity.created(uri).build();
	}
	
	
	
	@ApiOperation(value = "Atualiza um produto por ID")
	@RequestMapping(value="/{id}", method=RequestMethod.PUT)
	public ResponseEntity<Void> update(@RequestBody Produto produto, @PathVariable("id") Long id)  {
		produto.setId(id);
		produtoService.update(produto);
		return ResponseEntity.noContent().build();
	}
		
	
	
	@ApiOperation(value = "Remove um produto por ID")
	@RequestMapping(value="/{id}", method=RequestMethod.DELETE)
	public ResponseEntity<Void> delete(@PathVariable("id") Long id)  {
		produtoService.deleteById(id);
		return ResponseEntity.noContent().build();
	}
}
