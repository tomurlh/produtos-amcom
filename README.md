## Produtos AMCOM

API construida com Spring Framework para execução de teste da empresa AMCOM.

Abaixo a documentação das rotas/recursos disponíveis na mesma:

![](https://gitlab.com/tomurlh/produtos-amcom/raw/master/src/main/resources/documentacao.png)